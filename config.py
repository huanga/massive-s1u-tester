#!/usr/bin/python

# Numbers of UE
N_UE = 1

# Nodes address
SGW_Addr = "192.168.0.24"
PGW_Addr = "192.168.0.24"

# Interface ports
GTPv1_User_Port = 2152

# UE info
SGW_TEID_Start = 1
ENB_TEID_Start = 1
UE_IP_Start = "172.16.0.2"

# pinger settings
Packet_Size = 1436 # bytes
