#!/usr/bin/python

import gtp
import time
import json
import socket
import thread
import threading
import random
import logging
import Queue
import config
import struct
import socket
from ue import *
from scapy.all import *

lock = False # Lock input 
class ENB:
    "Class for ENB"

    instance = None
    gtpv1_u_socket = None
    UEThreads = dict()

    # TEID Table
    TEIDTable = dict()

    def __init__(self):

        # Start the GTPv1-U receiver thread
        ENB.gtpv1_u_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        ENB.gtpv1_u_socket.bind(('', 2152))
        thread.start_new_thread(gtpv1_u_thread, ())

        # Get a handle on the eNB object
        instance = self

def gtpv1_u_thread():
    logging.info("INFO|Started GTPv1-U connection")

    # Start listening for traffic from MME
    while True:
        # Listen for UDP data
        data, (ip, port) = ENB.gtpv1_u_socket.recvfrom(1024)

        # Make sure the socket is open and has returned valid results
        if not data:
            break

        # Spawn a new thread for the incoming packet
        thread.start_new_thread(handle_packet, (data, ip, port))

def handle_packet(packet, ip, port):
    # Decapsulate
    decap_data = gtp.decapsulate(packet)
    # Convert into more convenient scapy structure
    p = IP(decap_data['payload'])
    # Handle packet if necessary (ex: ping)
    if ICMP in IP(decap_data['payload']):
        packet = IP(src=p.src, dst=p.dst)/ICMP(type='echo-reply')
        send(packet, verbose=0)
    else:
        logging.info("Unidentified packet coming")
        return
    # Print info and return
	#logging.info("USER|Recv: ICMP Request [Src: %s] [Dst: %s]", p.src, p.dst)
    #logging.info("USER|Recv: %s:%d [TEID=0x%x]. Sent: %s:%d [TEID=0x%x].", \
        #ip, port, decap_data['teid'], data['next_ip'], coreinfo.GTPv1_User_Port, data['next_id'])
    return

def handleAddUE(num, ue_type, ping, interval=1, dst='8.8.8.8'):
    # Convert to correct type
    num = int(num)
    interval = int(interval)
    # Loop over number of UEs to create
    for i in range(int(num)):
    # Create a new UE and set values because no control plane
        ue = UE()
        ue.info['type'] = ue_type
        #ue.info['ip'] = '172.16.%d.%d' % ((ue.info['enb'] & 0xff), ((ue.info['id'] >> 8) & 0xff), (ue.info['id'] & 0xff))
#        ue.info['ip'] = '172.16.0.2'
#        ue.info['ul_sgw_id'] = config.SGW_TEID_Start
#        ue.info['dl_enb_id'] = config.ENB_TEID_Start
        ue.info['enb_ip'] = socket.gethostbyname(socket.gethostname())
        ue.info['sgw_ip'] = config.SGW_Addr
        ue.info['pgw_ip'] = config.PGW_Addr
        # Spawn a new thread for the created UE and let MEC know that it has been created
        ue_thread = UEThread(ue.info['id'], ue.info['ip'], ping, dst, interval, ue.info['ul_sgw_id'], ue.info['type'])
        ue_thread.daemon = True
        # Store the thread for future access and send request to MME
        ENB.UEThreads[ue.info['ip']] = ue_thread
        ue_thread.start()
        ue_thread.execute(ue_thread.awake)

def handleExit():
    ping_latency = 0
    ping_count = 0
    for ueIP, ueThread in ENB.UEThreads.items():
        if ueThread.ueType == 'pinger':
            if ueThread.ping_info['ping_count'] > 0.0:
                ping_latency += ueThread.ping_info['ping_time'] / ueThread.ping_info['ping_count']
                ping_count += 1
            if not ping_latency == 0:
                logging.info("Total Latency: %fs", (ping_latency/ping_count))
	ueThread.kill()

def handlePrintUEInfo(ueID):
    for ue in UE.ueList:
        if ue.info['id'] == ueID:
            # UE.ueList[(ueID-1)].printInfo()
            ue.printInfo()
            return
    print("No UE exists with given ID of %d" % ueID)

if __name__ == "__main__":

    # Set up logging module
    logging.basicConfig(filename='../logs/sgw.log', 
            filemode='a', 
            format='%(asctime)s|ENB|%(message)s', 
            datefmt='%Y-%m-%d %H:%M:%S',
            level=logging.INFO)
    # Define a handler that logs to the console
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # Set the simpler formatter for the console printout
    formatter = logging.Formatter('%(message)s')
    # Set the console format to the handler and connect the handler to the root logging
    logging.getLogger('').addHandler(console)

    # Start ENB 
    logging.info("")
    logging.info(" --> ENB Bootup <---")
    logging.info("")
    ENB()
    logging.info("INFO|Initialized and all interface servers are running")

    while True:
        if lock:
            continue
        cmd = raw_input("eNB > ").split()
        cmd = map(lambda x:x.lower(), cmd) # Make all commands lower case
        
        if len(cmd) > 0:
            if cmd[0] == "help":
                handlePrintHelp()
            elif cmd[0] == "add":
		handleAddUE(config.N_UE, "pinger", "e")
            elif cmd[0] == "exit":
                handleExit()
                break

