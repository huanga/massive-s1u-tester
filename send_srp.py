import threading
import time
import gtp
import multiprocessing
import socket
import numpy
from multiprocessing import *
from scapy.all import *

environ = sys.argv[1] #oai or ovs
N_UE = sys.argv[2]
TOTAL = sys.argv[3]
#UE info
TOTAL = int(TOTAL)
NUM_UE=int(N_UE)
START_IP="172.16.0.2"
TEID=1

#Setup info
LOCAL_IP="192.168.12.243"
SGW_IP="192.168.12.11"
SIZE_PAYLOAD=500 #bytes
DST_IP="8.8.8.8"
#DST_IP="216.58.205.206"
LOCAL_INTERFACE="eno1"
INTERVAL=0.01
PING_TIMEOUT=3 #sec
PING_TIMES=1

ip2int = lambda ipstr: struct.unpack('!I', socket.inet_aton(ipstr))[0]
int2ip = lambda n: socket.inet_ntoa(struct.pack('!I', n))

ue_list=[]
for i in range(NUM_UE):
	if i%100==0:
		print(START_IP, TEID)
	p = IP(src=START_IP, dst=DST_IP)/ICMP()/Raw(' '*int(SIZE_PAYLOAD))
	p = gtp.encapsulate(str(p),TEID)['message']
	p = Ether()/IP(src=LOCAL_IP, dst=SGW_IP)/UDP(sport=2152, dport=2152)/p
	ue_list.append(p)
	START_IP=int2ip(ip2int(START_IP)+1)
	TEID=TEID+1

rev_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
rev_socket.bind(('', 2152))
send_socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
send_socket.bind((LOCAL_INTERFACE,0))
def gtpv1_u_thread(index):
	# Start listening for traffic from MME
	while True:
		# Listen for UDP data
		data, (ip, port) = rev_socket.recvfrom(1024)
		# Make sure the socket is open and has returned valid results
		if not data:
			break
		# Spawn a new thread for the incoming packet
		thread.start_new_thread(handle_packet, (data, ip, port))

def handle_packet(packet, ip, port):
	# Decapsulate
	decap_data = gtp.decapsulate(packet)
	p = IP(decap_data['payload'])
	if ICMP in IP(decap_data['payload']):
		packet = Ether()/IP(src=p.src, dst=p.dst)/ICMP(type='echo-reply')
		#send_socket.send(str(packet))
		#send(packet, verbose=0)
	else:
		print("Unidentified packet coming")
		return

def send_data(index):
    #send_socket.send(str(ue_list[index]))
	start=time.clock()
	ans,unans = srp(ue_list[index], timeout=PING_TIMEOUT)
	end=time.clock()
	if not len(ans)==0:
		ue_time[index]+=(end-start)
		ue_counts[index]+=1
	#for res in ans:
	#	#q.put((index,res[1].time - res[0].sent_time))
	#	ue_time[index]+=(res[1].time - res[0].sent_time)
	#	ue_counts[index]+=1

if __name__ == "__main__":
	ue_time = multiprocessing.Array('f', NUM_UE)
	ue_counts = multiprocessing.Array('i', NUM_UE)
	#q=multiprocessing.Queue()
	pool = Pool(processes=100)
	print("Start 3 threads handling packets receiving\n")
	for i in xrange(0, 100):
		thread.start_new_thread(gtpv1_u_thread, (i,))
	print("Start sending ICMP packets\n")
	for i in xrange(0, PING_TIMES):
		pool.map(send_data, range(NUM_UE))
		time.sleep(INTERVAL)
		#Calculate results
	total_counts=PING_TIMES*NUM_UE
	latency=0
	c=0
	data=[]
	for i in xrange(0, NUM_UE):
		if not ue_counts[i]==0:
			latency = latency + (ue_time[i]/ue_counts[i])
			data.append(ue_time[i]/ue_counts[i])
			#print(data[-1])
			c = c + ue_counts[i]
		#print(data)
	sdev=numpy.std(data)
	filename = "./logs/iot/"+str(TOTAL)+"_ping_"+N_UE+"_"+environ+".log"
	ping_file = open(filename, 'w')
	ping_file.truncate()
	print("Average latency:%f, Ping drop: %f\n"%(latency/NUM_UE, float(total_counts-c)/float(total_counts)))
	#ping_file.write("Average latency:%f, Ping drop: %f\n"%(latency*1000/NUM_UE, float(total_counts-c)/float(total_counts)))
	ping_file.write("%f %f\n" % (latency*1000/NUM_UE, sdev/1000))
	#ping_file.write("Sdev: %f\n"%(sdev*1000))
	ping_file.close()
	pool.terminate()
	pool.join()
