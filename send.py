import threading
import time
import gtp
import multiprocessing
import socket
from multiprocessing import Pool
from scapy.all import *

environ = sys.argv[1] #oai or ovs
N_UE = sys.argv[2]
#UE info
NUM_UE=int(N_UE)
START_IP="172.16.0.2"
TEID=1
LOCAL_IP="192.168.12.243"
SGW_IP="192.168.12.11"
SIZE_PAYLOAD=1400 #bytes
DST_IP="8.8.8.8"
LOCAL_INTERFACE="eno1"
INTERVAL=0
ip2int = lambda ipstr: struct.unpack('!I', socket.inet_aton(ipstr))[0]
int2ip = lambda n: socket.inet_ntoa(struct.pack('!I', n))

ue_list=[]
for i in range(NUM_UE):        
	p = IP(src=START_IP, dst=DST_IP)/ICMP()/Raw(' '*int(SIZE_PAYLOAD))
	p = gtp.encapsulate(str(p),TEID)['message']
	p = Ether()/IP(src=LOCAL_IP, dst=SGW_IP)/UDP(sport=2152, dport=2152)/p
	ue_list.append(p)
	START_IP=int2ip(ip2int(START_IP)+1)
	TEID=TEID+1
	if i%100==0:
		print(START_IP, TEID)

send_socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW)
send_socket.bind((LOCAL_INTERFACE,0))

def send_data(index):
	send_socket.send(str(ue_list[index]))
	#ans = srp(ue_list[index], LOCAL_INTERFACE)
	#for res in ans:
	#	print(res[1].time - res[0].sent_time)

if __name__ == "__main__":
	pool = Pool(processes=100)
	for j in range(50000):
		result = pool.map(send_data, range(NUM_UE))
	pool.close()
	pool.terminate()
	pool.join()
#    int_pool = [1, 200, 500, 1000, 2000, 5000, 10000, 20000]
 #       time.sleep(INTERVAL)
