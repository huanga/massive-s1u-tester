#!/usr/bin/python

import gtp
import time
import json
import socket
import thread
import threading
import random
import logging
import Queue
import config
import struct
import socket
from enb import *
from scapy.all import *

ip2int = lambda ipstr: struct.unpack('!I', socket.inet_aton(ipstr))[0]
int2ip = lambda n: socket.inet_ntoa(struct.pack('!I', n))

class UE:
    "Class for UE creation"
     # Holder of all the created UEs so far
    ueCount = 1
    SGW_TEID = config.SGW_TEID_Start
    ENB_TEID = config.ENB_TEID_Start
    UE_IP = config.UE_IP_Start
    ueList = []
    def __init__(self):
        self.info = dict()
        self.info['id'] = UE.ueCount
        self.info['type'] = ''
        self.info['ip'] = UE.UE_IP
        self.info['ul_sgw_id'] = UE.SGW_TEID
        self.info['ul_pgw_id'] = 0
        self.info['dl_sgw_id'] = 0
        self.info['dl_enb_id'] = UE.ENB_TEID
        self.info['sgw_ip'] = ''
        # Append the UE list with the newly created UE
        UE.ueList.append(self)
        UE.ueCount += 1
        UE.SGW_TEID += 1
        UE.ENB_TEID += 1
        UE.UE_IP = int2ip(ip2int(UE.UE_IP)+1)
    def printInfo(self):
        print("UE%d [%s][%s]" % (self.info['id'], self.info['type'], self.info['ip']))

class UEThread(threading.Thread):
    "Class for UE threads"

    def __init__(self, ueID, ueIP, ping, dst, interval, sgwTEID, ueType):
        threading.Thread.__init__(self)
        self.q = Queue.Queue()
        self.k = False
        self.s = False

        self.dst = dst
        self.ueID = ueID
        self.ueIP = ueIP
        self.ueType = ueType
        self.sgwTEID = sgwTEID
        self.pingType = ping
        self.interval = interval

        self.busy = False

        # Ping information if needed
        self.ping_info = {'sgw_time': 0, 'sgw_count': 0,
                          'pgw_time': 0, 'pgw_count': 0,
                          'ext_time': 0, 'ext_count': 0,
                          'ping_time': 0, 'ping_count': 0 }

    def execute(self, function, *args, **kwargs):
        self.q.put((function, args, kwargs))

    def run(self):
        while not self.k:
            try:
                function, args, kwargs = self.q.get()
                function(*args, **kwargs)
            except Queue.Empty:
                pass

    def kill(self):
        self.k = True

    def stop(self):
        self.s = True

    def awake(self):
        self.busy = True
        while True:
            if self.ueType == 'firesensor':
                # Create a payload with SenML format
                payload = [
                    {'bn': 'FireSensor%d' % self.ueID, 'bt': 0},

                    {'n': 'temperature',   'u': 'cel', 'v': round((25+((10*random.random())-5)), 2)},
                    {'n': 'ionization',    'u': '1/m', 'v': round((6+((0.8*random.random())-0.4)), 2)},
                    {'n': 'photoelectric', 'u': 'cel', 'v': round((300+((100*random.random())-50)), 2)},
                    {'n': 'co',            'u': 'ppm', 'v': round((500+((60*random.random())-30)), 2)}
                ]
    
                packet = IP(src=self.ueIP, dst=self.dst)/UDP(sport=19990, dport=19991)/json.dumps(payload, separators=(',', ':'))
                packet = gtp.encapsulate(str(packet), self.sgwTEID)['message']
                packet = IP(dst=config.SGW_Addr)/UDP(sport=config.GTPv1_User_Port, dport=config.GTPv1_User_Port)/packet
                # Send the packet
                send(packet, verbose=0)

	    elif self.ueType == '1mb':
                dest = []
                if 'e' in self.pingType:
                    dest.append(self.dst)
                packet = IP(src=self.ueIP, dst=dest)/UDP(sport=19990, dport=19991)/Raw(' '*int(config.Packet_Size))
                packet = gtp.encapsulate(str(packet), self.sgwTEID)['message']
                packet = IP(dst=config.SGW_Addr)/UDP(sport=config.GTPv1_User_Port, dport=config.GTPv1_User_Port)/packet
            elif self.ueType == 'pinger':
                # Create list of the destinations to ping
                dest = []
                if 's' in self.pingType:
                    dest.append(config.SGW_Addr)
                if 'p' in self.pingType:
                    dest.append(config.PGW_Addr)
                if 'e' in self.pingType:
                    dest.append(self.dst)

                # Craft the packet using scapy
                packet = IP(src=self.ueIP, dst=dest)/ICMP()/Raw(' '*int(config.Packet_Size))
                packet = gtp.encapsulate(str(packet),self.sgwTEID)['message']
                packet = IP(dst=config.SGW_Addr)/UDP(sport=config.GTPv1_User_Port, dport=config.GTPv1_User_Port)/packet

                # Send ICMP echo-request to the destination and all intermediate steps
                ans, unans = sr(packet, verbose=0)
                # Get the results
                for res in ans:
                    # Check where the current ping answer came from and update accordingly
                    self.ping_info['ping_time'] += (res[1].time - res[0].time)
                    self.ping_info['ping_count'] += 1
            else:
                self.k = True

            # Sleep and update remainder counters
            count = self.interval
#            measured_latency = 0
#            measured_count = 0
            while count > 0:
                if self.k or self.s:
                    self.busy = False
                    self.s = False
                    return
                time.sleep(0.1)
                count = count - 0.1
#            for ueIP, ueThread in ENB.UEThreads.items():
#                if ueThread.ping_info['measured_count'] > 0.0:
#                    measured_latency += ueThread.ping_info['measured_time'] / ueThread.ping_info['measured_count']
#                    print(measured_latency)
#                    measured_count += 1
#            if not measured_latency == 0:
#                logging.info("Total Latency: %fs", (measured_latency/measured_count))
        self.busy = False
